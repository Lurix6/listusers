import React from 'react';
import './button.scss';
import classnames from 'classnames';


function Button(props) {
  const {
    children,
    variant,
    onClick,
    size,
    disabled,
    fullWidth,
    className
  } = props;

  const TYPES = {
    primary: 'primary',
    warning: 'warning',
    danger: 'danger',
    success: 'success',
    dark: 'dark',
  };

  const SIZES = {
    small: 'small',
    medium: 'medium',
    large: 'large',
  };


  const btnClass = classnames(
    { btn: true },
    [variant || TYPES.primary],
    [size || SIZES.medium],
    [!fullWidth || 'fullWidth'],
    className,
  );

  return (
    <button
      onClick={typeof onClick === 'function' ? onClick : null}
      className={btnClass}
      disabled={disabled}
    >
      {children}
    </button>
  );
}

export default Button;

import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from './home/Home';
import User from './user/User';
import PageNotFound from './pageNotFound/PageNotFound';


const App = () => {
  return (
    <Switch>
        <Route exact path="/" component={Home}/>
        <Route path="/user/:id" component={User}/>
        <Route component={PageNotFound}/>
    </Switch>
)
};

export default App;

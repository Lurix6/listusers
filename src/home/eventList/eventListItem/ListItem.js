import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import './ListItem.scss';
import Button from '../../../components/Button';
import { deleteUserById, updateUserData } from '../../HomeActions';


class ListItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      change: false,
      newUserData: {
        id: props.el.id,
        name: props.el.name,
        age: props.el.age,
      },
    };
  }

    changeUser = el => (
      <div className="listItemRoot">
          <div className="changeUserData">
            <p className="changeUserData-p">{el.id}</p>
            <input className="changeUserData-input" defaultValue={el.name} onChange={ev => this.setState({ newUserData: { ...this.state.newUserData, name: ev.target.value } })} />
            <input className="changeUserData-input" defaultValue={el.age} onChange={ev => this.setState({ newUserData: { ...this.state.newUserData, age: ev.target.value } })} />
          </div>
        <div className="btnChange">
          <Button size="large" className="hello" variant="dark" onClick={() => this.setState({ change: false })}>Відмінити</Button>
          <Button variant="primary" onClick={this.changeUserData}>Зберегти</Button>
        </div>
      </div>
    );

  changeUserData = () => {
    this.setState({ change: false });
    const newData = { ...this.state.newUserData, age: parseInt(this.state.newUserData.age) };
    this.props.updateUserData(newData);

    const users = JSON.parse(localStorage.getItem('users'));
    let newId;
    for (let i = 0; i < users.length; i++) {
      if (users[i].id === this.state.newUserData.id)newId = i;
    }
    users[newId] = this.state.newUserData;
    localStorage.setItem('users', JSON.stringify(users));
  };

  aboutUser = el => (
      <div className="listItemRoot">

          <div className="aboutUser">
            <p>{el.id}</p>
            <p>{el.name}</p>
            <p>{el.age}</p>
          </div>

        <div className="btnChange">
          <Link to={`/user/${el.id}`}>
            <Button variant="success">Open</Button>
          </Link>
          <Button size="large" className="hello" variant="dark" onClick={() => this.setState({ change: true })}>Редагувати</Button>
          <Button variant="primary" onClick={() => this.deleteUser(el.id)}>Видалити</Button>
        </div>
      </div>
  );


  deleteUser = (id) => {
    this.props.deleteUserById(id)
    const list = JSON.parse(localStorage.getItem('users'));
    let newList = list.filter(el => el.id !== id);
    localStorage.setItem('users', JSON.stringify(newList));
  };

  render() {
    const { el } = this.props;
    return (
      <div>
        { this.state.change ? this.changeUser(el) : this.aboutUser(el) }
      </div>
    );
  }
}

ListItem.propTypes = {
  el: PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    age: PropTypes.number,
  }).isRequired,
  updateUserData: PropTypes.func.isRequired,
};

export default connect(null, { deleteUserById, updateUserData })(ListItem);

import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropType from 'prop-types';
import './EventList.scss';
import ListItem from './eventListItem/ListItem';
import { defaultUserList } from '../HomeConstants';
import { initializationNewUserList } from '../HomeActions';

class EventList extends Component {
  componentDidMount() {
    if (localStorage.getItem('users')) {
      this.props.initializationNewUserList(JSON.parse(localStorage.getItem('users')));
    } else {
      localStorage.setItem('users', JSON.stringify(defaultUserList));
    }
  }

  render() {
    const { users } = this.props;
    return (
      <div className="App">
        {
            users && users.map(el => <ListItem changeUser={this.changeUser} key={el.id} el={el} />)
          }
      </div>
    );
  }
}

EventList.propTypes = {
  users: PropType.arrayOf(PropType.shape({
    id: PropType.number.isRequired,
    name: PropType.string,
    age: PropType.number,
  })).isRequired,
  initializationNewUserList: PropType.func.isRequired,

}

export default connect(state => ({
  users: state.users,
}), { initializationNewUserList })(EventList);

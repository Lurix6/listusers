export const DELETE_USER = 'DELETE_USER';

export const UPDATE_USER_DATA = 'UPDATE_USER_DATA';

export const INITIALIZATION_NEW_USER_LIST = 'INITIALIZATION_NEW_USER_LIST';

export const ADD_NEW_USER = 'ADD_NEW_USER';

export const defaultUserList = [
  { id: 0, name: 'Martin', age: 21 },
  { id: 1, name: 'Roma', age: 21 },
  { id: 2, name: 'Mark', age: 21 },
  { id: 3, name: 'Dima', age: 21 },
];

import React, { Component } from 'react';
import { connect } from 'react-redux';
import './AddItemBlock.scss';
import { addNewUser } from '../HomeActions';
import Button from '../../components/Button';

const MAX_AND_MIN_AGE = {
  max: 120,
  min: 0,
};

class AddItemBlock extends Component {
    state = {
      id: 0,
      name: '',
      age: 0,

    };

  changeAge =(ev) => {
    const age = parseInt(ev.target.value);
    if (age < MAX_AND_MIN_AGE.max && age > MAX_AND_MIN_AGE.min) this.setState({ age });
  };

  changeName =(ev) => {
    const name = ev.target.value;
    if (name.length >= 0) this.setState({ name });
  };

  changeId =(ev) => {
    const id = parseInt(ev.target.value);
    if (id > 0)this.setState({ id });
  };

  createNewItem =() => {
    if (this.state.age && this.state.name && !this.isIdBusy(this.state.id)) {
      this.props.addNewUser(this.state);
      const lisUser = JSON.parse(localStorage.getItem('users'));
      localStorage.setItem('users', JSON.stringify([...lisUser, this.state]));
    }
  };

  isIdBusy =(id) => {
    const { users } = this.props;
    if (users.length === 0) return false;
    for (let i = 0; i < users.length; i++) {
      if (users[i].id === id) return true;
    }
    return false;
  }

  render() {
    return (
      <div className="addNewItemRoot">
        <span className="addNewItem-title">Add new item</span>
        <div className="addNewItem-inputsBlock">
          <div>
            <p>id:</p>
            <input value={this.state.id} type="number" className="addNewItem-inputId" onChange={this.changeId} />
          </div>
          <div>
            <p>New name:</p>
            <input onChange={this.changeName} />
          </div>
          <div>
            <p>New age:</p>
            <input onChange={this.changeAge} type="number" min={MAX_AND_MIN_AGE.min} max={MAX_AND_MIN_AGE.max} className="addNewItem-inputAge" />
          </div>
        </div>
        <div className="addNewItem-buttonBlock">
          <Button className="saveBtn" variant="primary" onClick={this.createNewItem}>Зберегти</Button>
        </div>
      </div>
    );
  }
}


AddItemBlock.propTypes = {
};

export default connect(state => ({
  users: state.users,
}), { addNewUser })(AddItemBlock);


// <p className="addNewItem-id">id</p>

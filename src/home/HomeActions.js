import { DELETE_USER, UPDATE_USER_DATA, INITIALIZATION_NEW_USER_LIST, ADD_NEW_USER } from './HomeConstants';

export function deleteUserById(id) {
  return {
    type: DELETE_USER,
    payload: { id },
  };
}


export function updateUserData(user) {
  return {
    type: UPDATE_USER_DATA,
    payload: { user },
  };
}


export function initializationNewUserList(userList) {
  return {
    type: INITIALIZATION_NEW_USER_LIST,
    payload: { userList },
  };
}


export function addNewUser(newUser) {
  return {
    type: ADD_NEW_USER,
    payload: { newUser },
  };
}

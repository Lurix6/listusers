import React from 'react';
import EventList from './eventList/EventList';
import AddItemBlock from './addItemBlock/AddItemBlock';

const Home = () => (
  <div>
        <EventList />
        <AddItemBlock />
  </div>
);

export default Home;

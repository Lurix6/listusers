import { DELETE_USER, UPDATE_USER_DATA, INITIALIZATION_NEW_USER_LIST, ADD_NEW_USER } from './HomeConstants';


const defaultUserList = [
  { id: 0, name: 'Martin', age: 21 },
  { id: 1, name: 'Roma', age: 21 },
  { id: 2, name: 'Mark', age: 21 },
  { id: 3, name: 'Dima', age: 21 },
];

function immutableChangeItemById(list, item, id) {
  const newList = list.slice();
  let newId;
  for (let i = 0; i < newList.length; i++) {
    if (newList[i].id === id)newId = i;
  }
  newList[newId] = item;

  return newList;
}


export default (userList = defaultUserList, action) => {
  const { type, payload } = action;
  switch (type) {
    case INITIALIZATION_NEW_USER_LIST: return payload.userList;
    case DELETE_USER: return userList.filter(el => el.id !== payload.id);
    case UPDATE_USER_DATA: return immutableChangeItemById(userList, payload.user, payload.user.id);
    case ADD_NEW_USER: return [...userList, payload.newUser];
    default: return userList;
  }
};

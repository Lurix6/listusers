import { createHashHistory } from 'history';

const history = createHashHistory();

export const appHistory = history;

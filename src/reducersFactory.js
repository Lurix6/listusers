import { combineReducers } from 'redux';
import users from './home/HomeReducers';

export default combineReducers({
  users,
});

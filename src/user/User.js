import React from 'react';
import './User.scss';
import { connect } from "react-redux";
import Button from '../components/Button/'


const getUserById = (users, id) => {
  for (let i = 0; i < users.length; i++) {
    if (users[i].id === id) {
      return users[i];
    }
  }
  return false;
};


const User = (props) => {
  const user =  getUserById(props.users, parseInt(props.match.params.id));

  return (
    <div className="root">
      { user ?
        <div className="userRoot">
            <p>User</p>
          <p>name : {user.name}</p>
          <p>id: {user.id}</p>
          <p>age: {user.age}</p>
            <Button onClick={ () => props.history.push('/')} fullWidth>Back</Button>
        </div> :
        <div>
          <p>User not found</p>
        </div> }
    </div>
  );
};

export default connect(state => ({
  users: state.users,
}))(User);